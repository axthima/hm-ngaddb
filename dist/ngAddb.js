(function(window, document) {

// Create all modules and define dependencies to make sure they exist
// and are loaded in the correct order to satisfy dependency injection
// before all nested files are concatenated by Grunt

// Config
angular.module('ngAddb.config', [])
    .value('ADDBConfig', {
        debug: true,
        apiKey:"4839cf34a145461ca5dca20d0049d653"
    });

// Modules
angular.module('ngAddb.services', []);
angular.module('ngAddb',
    [
        'ngAddb.config',
        'ngAddb.services'
    ]);
angular.module('ngAddb.services')
	.service('ADDBBusiness', ['ADDBConfig', '$q', '$rootScope', function(ADDBConf, $q, $rootScope) {
		var _this = this;

		addb.UrlBuilder.prototype._addParameter = addb.UrlBuilder.prototype.addParameter
		addb.UrlBuilder.prototype.addParameter = function(name, value) {
			if(name == 'appId') {
				return this._addParameter('apiKey', value)
			}
			return this._addParameter(name, value);
		}


		addb.init({
		    appId: ADDBConf.apiKey
		});

		var _businessReady = $q.defer();
		_this.businessReady = _businessReady.promise;

		var database = []
		downloadDatabase = function () {
			var q1 = $q.defer();
			var q2 = $q.defer();
			var q3 = $q.defer();
			var q4 = $q.defer();
			addb.drinks().withType('vodka').rating('gt99').forOccasion('evening').loadSet(function(res) {
				database = database.concat(res.result)
				q1.resolve()
				$rootScope.$apply()
			})
			addb.drinks().withType('whisky').rating('gt60').forOccasion('evening').loadSet(function(res) {
				database = database.concat(res.result)
				q2.resolve()
				$rootScope.$apply()
			})
			addb.drinks().withType('tequila').rating('gt40').forOccasion('evening').loadSet(function(res) {
				database = database.concat(res.result)
				q3.resolve()
				$rootScope.$apply()
			})
			addb.drinks().withType('rum').rating('gt50').forOccasion('evening').loadSet(function(res) {
				database = database.concat(res.result)
				q4.resolve()
				$rootScope.$apply()
			})
			$q.all([q1.promise,q2.promise,q3.promise,q4.promise]).then(function() {
				_businessReady.resolve();
			})
		}
		downloadDatabase();

		this.getDatabase = function(limit) {
			return database.slice(0, limit);
		}

		this.getForId = function(id) {
			for(var i in database) {
				if(database[i].id == id) {
					return database[i];
				}
			}
		}
	}]);})(window, document);