// Create all modules and define dependencies to make sure they exist
// and are loaded in the correct order to satisfy dependency injection
// before all nested files are concatenated by Grunt

// Config
angular.module('ngAddb.config', [])
    .value('ADDBConfig', {
        debug: true,
        apiKey:"4839cf34a145461ca5dca20d0049d653"
    });

// Modules
angular.module('ngAddb.services', []);
angular.module('ngAddb',
    [
        'ngAddb.config',
        'ngAddb.services'
    ]);
