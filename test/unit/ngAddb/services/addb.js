'use strict';

// Set the jasmine fixture path
// jasmine.getFixtures().fixturesPath = 'base/';

describe('services.business', function() {

    var business;

    var async = new AsyncSpec(this);

    beforeEach(module('ngAddb.config'));
    beforeEach(module('ngAddb.services'));

    async.beforeEach(function(done) {
        inject(function(ADDBBusiness, $q) {
            business = ADDBBusiness;
            $q.all([business.businessReady]).then(function() {
                done();
            })
        })
    });
    
    async.it('should get for id', function(done) {
        expect(business.getForId('tequila-sunrise').id).toEqual('tequila-sunrise')
        done()
        
    });

});
